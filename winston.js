const winston = require('winston');

const options = {
    file: {
        level: 'debug', // change this to info after dev ended
        filename: `logs/combined.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: false,
    },
    // console: {
    //     level: 'debug',
    //     handleExceptions: true,
    //     json: false,
    //     colorize: true,
    // },
};

const logger = winston.createLogger({
    transports: [
        new winston.transports.File(options.file),
        new winston.transports.Console(options.file)
    ]
});

exports.logger = logger;
