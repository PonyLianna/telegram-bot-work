const auth = require('./auth');
const cart = require('./cart');

module.exports = async function (bot, ctx) {
    if (ctx.message) {
        await auth(bot, ctx)
    }
    cart(bot, ctx);
};


