const Markup = require('telegraf/markup');

const clearUser = {
    auth: false,
    id: '',
    phone: '',
    name: '',
    bday: '',
    state: 0,
    password: ''
};

module.exports = async function (bot, ctx) {
    // Если в массиве отсутствует упоминание о пользователе - Добавить этого пользователя
    if (!ids[ctx.message.from.id]) {
        console.log('Added to list');
        ids[ctx.message.from.id] = clearUser;
    }

    // Если пользователь авторизован - поприветствовать
    if (!ids[ctx.message.from.id].auth) {
        console.log(1);
        if (ctx.message.text.match(/[Зз][Аа][Пп][Оо][Мм][Нн][Ии][Тт][Ьь][ ][Мм][Ее][Нн][Яя]/g)) {
            if (ids[ctx.message.from.id].auth) {
                ctx.reply('Вы уже авторизованы!');
            } else {
                ctx.reply('Запомнить меня', {
                    parse_mode: "Markdown", reply_markup: Markup
                        .keyboard(['Авторизация', 'Регистрация', 'Выйти'], {columns: 3})
                        .resize()
                });
            }
        } else if (ctx.message.text.match(/[Вв][Ыы][Йй][Тт][Ии]/g)) {
            console.log('Выхожу');
            ids[ctx.message.from.id] = null;
            ctx.reply('Выхожу', {
                parse_mode: "Markdown", reply_markup: Markup
                    .keyboard(['🛒 Меню', '📝 Запомнить меня'], {columns: 2})
                    .resize()
            });
        } else if (ctx.message.text.match(/[Аа][Вв][Тт][Оо][Рр][Ии][Зз][Аа][Цц][Ии][Яя]/g)) {
            let result = await ctx.reply('Авторизуюсь', {
                parse_mode: "Markdown", reply_markup: Markup
                    .keyboard(['Выйти'], {columns: 2})
                    .resize()
            });

            console.log(result);

            ids[ctx.message.from.id] = {
                auth: false,
                state: 1,
                authState: 1
            };

            await ctx.reply('Пожалуйста, введите номер вашего телефона (пример: `903453232`)', {parse_mode: "Markdown"});

        } else if (ctx.message.text.match(/[Рр][Ее][Гг][Ии][Сс][Тт][Рр][Аа][Цц][Ии][Яя]/g)) {
            await ctx.reply('Регистрируясь в данном сервисе вы подтверждаете свое совершеннолетие и соглашаетесь с [правилами](https://www.cardnonstop.com/oferta-medved.html)', {
                parse_mode: "Markdown", reply_markup: Markup
                    .keyboard(['Выйти'], {columns: 2})
                    .resize()
            });

            ids[ctx.message.from.id] = {
                id: '',
                name: '',
                phone: '',
                bday: '',
                password: '',
                auth: false,
                state: 1,
            };

            await ctx.reply('Пожалуйста, введите номер вашего телефона (пример: `903453232`)', {parse_mode: "Markdown"});

        } else {
            console.log(ids);
            let unique_id = ctx.message.from.id; // уникальный id пользовательского чата
            switch (ids[unique_id].state) {
                case 1:
                    if (ctx.message.text.match(/^(([0-9]){10})$/g)) {
                        ids[unique_id].phone = ctx.message.text;
                        if (ids[unique_id].authState) {
                            ids[unique_id].state = 5;
                            await ctx.reply('Введите пароль (от _5_ до _15_ символов *без пробела*)', {parse_mode: "Markdown"});
                        } else {
                            ids[unique_id].state += 1;
                            console.log(ids[unique_id].state);
                            await ctx.reply('Пожалуйста введите свое имя без использования *спец. знаков*', {parse_mode: "Markdown"});
                        }
                    } else {
                        await ctx.reply('Вы неправильно ввели свой номер телефона или не в том формате');
                        await ctx.reply('Пожалуйста, попробуйте еще раз');
                    }

                    break;

                case 2:
                    ids[unique_id].name = ctx.message.text.replace(/[^\w\s]/gi, '');
                    ids[unique_id].state += 1;
                    console.log(ids[unique_id].state);
                    await ctx.reply('Введите свою дату рождения в формате _"день-месяц-год"_ (пример: `03-12-1998`)', {parse_mode: "Markdown"});
                    break;

                // case 3:
                //     if (ctx.message.text.match(/^(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[0-2])-(19|20)\d{2}$/g)) {
                //         ids[unique_id].bday = ctx.message.text;
                //         ids[unique_id].state += 1;
                //         console.log(ids[unique_id].state);
                //         await ctx.reply("Вы подтверждаете свое совершеннолетие и соглашаетесь с [правилами](https://www.cardnonstop.com/oferta-medved.html)?", {
                //             parse_mode: "Markdown", reply_markup: Markup
                //                 .keyboard(['Да', 'Нет', 'Выйти'], {columns: 2})
                //                 .resize()
                //         });
                //     } else {
                //         ctx.reply('Дата рождения введена неправильно, попробуйте еще раз!');
                //     }
                //
                //     break;

                case 3:
                    console.log(ctx.message.text);
                    if (ctx.message.text.match(/^(0?[1-9]|[12][0-9]|3[01])-(0?[1-9]|1[0-2])-(19|20)\d{2}$/g)) {
                                ids[unique_id].bday = ctx.message.text;
                                ids[unique_id].state += 1;
                                console.log(ids[unique_id].state);
                        await ctx.reply('Введите пароль (от _5_ до _15_ символов *без пробела*)', {
                            parse_mode: "Markdown", reply_markup: Markup
                                .keyboard(['Выйти'], {columns: 2})
                                .resize()
                        });
                    } else if (ctx.message.text.match(/[Нн][Ее][Тт]/g)) {
                        ids[unique_id].als18Year = false;
                        ids[unique_id].state += 1;
                        await ctx.reply('Введите пароль (от _5_ до _15_ символов *без пробела*)', {
                            parse_mode: "Markdown", reply_markup: Markup
                                .keyboard(['Выйти'], {columns: 2})
                                .resize()
                        });
                    }
                    break;

                case 4:
                    ids[unique_id].password = ctx.message.text;
                    ids[unique_id].state += 1;
                    await ctx.reply('Повторите пароль');
                    break;

                case 5:
                    if (ids[unique_id].authState) {
                        ids[unique_id].password = ctx.message.text;
                        let login = await clientsvc.clientConnect(ids[unique_id].phone, ids[unique_id].password);
                        console.log(login);
                        if (login.ClientConnectResult) {
                            ctx.reply(`Добро пожаловать! ${login.aName}`, {
                                reply_markup: Markup
                                    .keyboard(['🛒 Меню', '📃 Корзина'], {columns: 2})
                                    .resize()
                            });

                            ids[unique_id].auth = true;
                            ids[unique_id].id = login.aID;
                            ids[unique_id].name = login.aName;
                            ids[unique_id].als18Year = login.a18Year;
                        } else {
                            ctx.reply('Пароль или имя пользователя введены неправильно', {
                                parse_mode: "Markdown", reply_markup: Markup
                                    .keyboard(['Авторизация', 'Регистрация', 'Выйти'], {columns: 3})
                                    .resize()
                            });
                            ids[unique_id].state = 1;
                        }
                    } else if ((ids[unique_id].password !== ctx.message.text)) {
                        ids[unique_id].state -= 1;
                        ids[unique_id].password = '';
                        await ctx.reply('Введите пароль');
                    } else {
                        let client = await clientsvc.addClient(
                            ids[unique_id].name,
                            ids[unique_id].phone,
                            ids[unique_id].password,
                            ids[unique_id].bday,
                            '',
                            '',
                            '',
                            true)
                            .catch(async err => {
                                await ctx.reply('Произошла ошибка, повторите еще раз');
                                ids[ctx.message.from.id].state = 0;
                            });
                        if (client) {
                            logger.debug(ids[unique_id]);
                            ids[ctx.message.from.id].auth = true;
                            ids[ctx.message.from.id].id = client.aID;
                            ctx.reply(`Добро пожаловать! ${ids[ctx.message.from.id].name}`, {
                                reply_markup: Markup
                                    .keyboard(['🛒 Меню', '📃 Корзина'], {columns: 2})
                                    .resize()
                            });
                        } else {
                            await ctx.reply('Данная запись уже существует', {
                                reply_markup: Markup
                                    .keyboard(['Авторизация', 'Регистрация', 'Выйти'], {columns: 3})
                                    .resize()
                            });

                            ids[ctx.message.from.id] = {auth: false, state: 0};
                        }
                    }
                    break;
            }
        }
    } else if (ids[ctx.message.from.id].auth &&
        (ctx.message.text.match(/[Зз][Аа][Пп][Оо][Мм][Нн][Ии][Тт][Ьь][ ][Мм][Ее][Нн][Яя]/g)) ||
        ctx.message.text.match(/[Аа][Вв][Тт][Оо][Рр][Ии][Зз][Аа][Цц][Ии][Яя]/g) ||
        ctx.message.text.match(/[Рр][Ее][Гг][Ии][Сс][Тт][Рр][Аа][Цц][Ии][Яя]/g)) {

        await ctx.reply(`Вы уже авторизованы, ${ids[ctx.message.from.id].name}!`, {
            reply_markup: Markup
                .keyboard(['🛒 Меню', '📃 Корзина'], {columns: 2})
                .resize()
        });
    }
};