const Telegraf = require('telegraf');
const Markup = require('telegraf/markup');

module.exports = async function (bot, ctx) {
    if (ctx.message) {
        logger.debug(ctx.message);
        if (ctx.message.text.match(/[мМ][еЕ][нН][юЮ]|[[\u{1f6d2}]]/g)) {
            const category = await categorysvc.getCategoryDataList();
            const newCategory = [];

            for await(element of category){if (await productsvc.getProductsDataList(element)) newCategory.push(element);}
            logger.debug(newCategory);

            const buttons = newCategory.map(item => Telegraf.Markup.callbackButton(item.CatName, `catname${item.ID}`));
            await ctx.reply('Выберите категорию', Telegraf.Extra
                .HTML()
                .markup((m) => m.inlineKeyboard(buttons)));
        } else if (ctx.message.text.match(/[Кк][Оо][Рр][Зз][Ии][Нн][Аа]/g) && ids[ctx.message.from.id].auth) {
            let datalist = await productsvc.getProdClientsDataList(ids[ctx.message.from.id].id, false, false);
            if (datalist) {
                const products = datalist.ProdClientsData;

                const result = await ctx.reply('Меню', {
                    reply_markup: Markup
                        .keyboard(['Оформить', 'Вернуться'], {columns: 2})
                        .resize()
                });

                const buttons = products.map(item => Telegraf.Markup.callbackButton(`${item.ProdName}  ${item.ProdCount}шт. - ${item.ProdSum}р.`, `cart${item.ID} ${item.ProdCount} ${result.message_id + 1}`));
                const massive = products.map((item) => Number(item.ProdSum));

                const sum = massive.reduce((acc, currentItem) => acc + currentItem);
                await ctx.reply(`Корзина с суммой ${sum}р.`, Telegraf.Extra
                    .HTML()
                    .markup((m) => m.inlineKeyboard(buttons, {columns: 1})));

            } else await ctx.reply('Ваша корзина пуста! Попробуйте набрать товары.');

        } else if (ctx.message.text.match(/[Оо][Фф][Оо][Рр][Мм][Ии][Тт][Ьь]/g) && ids[ctx.message.from.id].auth) {

            const products = await productsvc.getProdClientsDataList(ids[ctx.message.from.id].id, false, false);

            logger.debug(`${products}`);
            if (products){
                const requestNumb = products.ProdClientsData[0].RequestNumb;

                const ispay = await productsvc.isPayShoppingCart(requestNumb, 1);

                logger.debug(`IsPay: ${ispay}`);

                const textProducts = products.ProdClientsData.map(item => `${item.ProdName} в количестве ${item.ProdCount} на сумму **${item.ProdSum}**р.\n`);
                const massive = products.ProdClientsData.map((item) => Number(item.ProdSum));

                const sum = massive.reduce((acc, currentItem) => acc + currentItem);

                const text = `${textProducts.join('')}__Заказ на сумму ${sum}р.__ оформлен и будет ждать вас на кассе. Вы сможете оплатить его без очереди, обратившись к бармену. **Борисовский проезд, 24с2**`;
                await ctx.reply(text, {
                    parse_mode: "Markdown",
                    reply_markup: Markup
                        .keyboard(['🛒 Меню', '📃 Корзина'], {columns: 2})
                        .resize()
                })
            } else ctx.reply('Ваша корзина пуста! Попробуйте набрать товары');
        } else if (ctx.message.text.match(/[Вв][Ее][Рр][Нн][Уу][Тт][Ьь][Сс][Яя]/g) && ids[ctx.message.from.id].auth) {
            ids[ctx.message.from.id].state = 19;
            await ctx.reply(`Вы вернулись в главное меню, ${ids[ctx.message.from.id].name}`, {
                reply_markup: Markup
                    .keyboard(['🛒 Меню', '📃 Корзина'], {columns: 2})
                    .resize()
            });
        }

    }

    if (ctx.update.callback_query) {
        if (ctx.update.callback_query.data.match(/catname/g)) {
            const data = ctx.update.callback_query.data.replace(/catname/g, '');
            const products = await productsvc.getProductsDataList(data);
            let buttons = products.slice(0, 8).map(item => Telegraf.Markup.callbackButton(`${item.ProdName} - ${item.Price}р.`, `product${item.ID}`));

            if (products.length > 9) buttons.push(Telegraf.Markup.callbackButton("Вперед", `next1 ${data}`));

            await ctx.reply(`Выберите товар`, Telegraf.Extra
                .HTML()
                .markup((m) => m.inlineKeyboard(buttons, {columns: 1})));
        }

        if (ctx.update.callback_query.data.match(/next/g)) {
            const data = ctx.update.callback_query.data.replace(/next/g, '').split(' ');
            console.log(data);
            console.log(ctx.update.callback_query.data);

            // Получаем список товаров
            const products = await productsvc.getProductsDataList(data[1]);

            // Формируем меню с длинной в 8 пунктов
            const buttons = products.slice(8 * parseInt(data[0]), 8 * parseInt(data[0]) + 8)
                .map(item => Telegraf.Markup.callbackButton(`${item.ProdName} - ${item.Price}р.`, `product${item.ID}`));


            if (parseInt(data[0]) === 0) buttons.push(Telegraf.Markup.callbackButton("Вперед", `next1 ${data[1]}`));
            else {
                if (buttons.length > 7) buttons.push(Telegraf.Markup.callbackButton("Вперед", `next${parseInt(data[0]) + 1} ${data[1]}`),
                        Telegraf.Markup.callbackButton("Назад", `next${parseInt(data[0]) - 1} ${data[1]}`));
                else buttons.push(Telegraf.Markup.callbackButton("Назад", `next${parseInt(data[0]) - 1} ${data[1]}`));
            }
            await ctx.reply(`Выберите товар`, Telegraf.Extra
                .HTML()
                .markup((m) => m.inlineKeyboard(buttons, {columns: 1})));
        }

        if (ids[ctx.update.callback_query.from.id].auth) {
            if (ctx.update.callback_query.data.match(/product/g)) {
                await productsvc.addShoppingCart(ids[ctx.update.callback_query.from.id].id, ctx.update.callback_query.data.replace(/product/g, ''), 1);
                await ctx.answerCbQuery('Добавлено в корзину');
            }
            if (ctx.update.callback_query.data.match(/cart/g)) {
                let elements = ctx.update.callback_query.data.replace(/cart/g, '').split(' ');

                if (parseInt(elements[1]) === 1) await productsvc.delShoppingCart(elements[0]);
                else await productsvc.editShoppingCart(elements[0], parseInt(elements[1]) - 1);


                let datalist = await productsvc.getProdClientsDataList(ids[ctx.update.callback_query.from.id].id, false, false);
                if (datalist) {
                    const products = datalist.ProdClientsData;

                    const buttons = products.map(item => Telegraf.Markup.callbackButton(`${item.ProdName} ${item.ProdCount}шт. - ${item.ProdSum}р.`, `cart${item.ID} ${item.ProdCount} ${elements[2]}`));
                    const massive = products.map((item) => Number(item.ProdSum));
                    const sum = massive.reduce((acc, currentItem) => acc + currentItem);

                    bot.telegram.editMessageText(ctx.update.callback_query.from.id, elements[2], '', `Корзина с суммой ${sum}р.`, Telegraf.Extra
                        .HTML()
                        .markup((m) => m.inlineKeyboard(buttons, {columns: 1})));
                } else bot.telegram.editMessageText(ctx.update.callback_query.from.id, elements[2], '', 'Ваша корзина пуста! Попробуйте набрать товары.');
                await ctx.answerCbQuery('Убрано из корзины');
            }
        }
    }
};