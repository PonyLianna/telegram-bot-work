const soap = require('soap');
const logger = require('../winston').logger;

const path = require('path');
const scriptName = path.basename(__filename);

let client;

exports.init = async function () {
    const wdsl = 'https://beer.graysonhunt.ru/CategorySvc.asmx?wsdl';
    client = await soap.createClientAsync(wdsl);
    logger.debug('Active: ' + wdsl);
};

/**
 * Функция получения категорий товаров
 * @returns {string}
 **/
exports.getCategoryDataList = async function () {
    logger.info(`${scriptName} ${Object.keys(this)[1]}`);
    return (await client.GetCategoryDataListAsync())[0].GetCategoryDataListResult.CategoryData;
};

