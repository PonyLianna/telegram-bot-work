const soap = require('soap');
const logger = require('../winston').logger;

let client;
exports.init = async function () {
    const wdsl = 'https://beer.graysonhunt.ru/ProductsSvc.asmx?wsdl';
    client = await soap.createClientAsync(wdsl);
    logger.debug('Active: ' + wdsl);
};

const path = require('path');
const scriptName = path.basename(__filename);

/**
 * Функция получения продуктов по категории товара
 * @param aCatId - номер категории
 * @returns {array}
 *
 * ProductsData
 **/
exports.getProductsDataList = async function (aCatId) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aCatId: ${aCatId}}`);
    const func = ((await client.GetProductsDataListAsync({aCatID: aCatId}))[0].GetProductsDataListResult);
    return  func ? func.ProductsData : null;
};

/**
 * Добавление нового товара в корзину клиента (покупателя)
 * @param aClID клиентайди
 * @param aProdID
 * @param aProdCount
 * @returns {array}
 *
 * AddShoppingCartResult - состояние
 * aID - id номера заказа
 * aRequestNumb    - уникальный номер оплаты
 * aSumProd    - сумма покупок
 * aDiscSumProd - сумма со скидкой
 * aSumProdCount - количество покупок
 */
exports.addShoppingCart = async function (aClID, aProdID, aProdCount) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aClID: ${aClID}, aProdID: ${aProdID}, aProdCount: ${aProdCount}}`);
    return (await client.AddShoppingCartAsync({aClID: aClID, aProdID: aProdID, aProdCount: aProdCount}))[0];
};

/**
 * Редактирование количества товара в корзине клиента (покупателя)
 * @param aID
 * @param aProdCount
 * @returns {array}
 *
 * EdtShoppingCartResult - состояние
 * aSumProd    - сумма заказа
 * aDiscSumProd    - сумма с учетом скидки
 * aSumProdCount - количество покупок
 */
exports.editShoppingCart = async function (aID, aProdCount) {
    logger.info(`${scriptName} ${Object.keys(this)[1]}`);
    logger.debug(`{aID: ${aID}, aProdCount: ${aProdCount}}`);
    return (await client.EdtShoppingCartAsync({aID: aID, aProdCount: aProdCount}))[0];
};

/**
 * Полное удаление товара из корзины клиента (покупателя)
 * @param aID
 * @returns {array}
 *
 * DelShoppingCartResult - состояние
 * aSumProd    - сумма
 * aDiscSumProd
 * aSumProdCount
 */
exports.delShoppingCart = async function (aID) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aID: ${aID}}`);
    return (await client.DelShoppingCartAsync({aID: aID}))[0];
};

/**
 * Установка признака ОПЛАЧЕНО для всех неоплаченных товаров (заказа) в корзине клиента (покупателя)
 * @param aRequestNumb
 * @param aIsPay
 * @returns {array}
 *
 * Примечание:  Нельзя закрывать заказ без выставления признака "Оплачено"
 */
exports.isPayShoppingCart = async function (aRequestNumb, aIsPay) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aRequestNumb: ${aRequestNumb}, aIsPay: ${aIsPay}}`);
    return (await client.IsPayShoppingCartAsync({
        aRequestNumb: aRequestNumb,
        aIsPay: aIsPay
    }))[0];
};

/**
 * Установка признака ТОВАР ОТПУЩЕН для оплаченного заказа клиента (покупателя)
 * @param aRequestNumb
 * @param aIsPay
 * @returns {array}
 */
exports.isCloseShoppingCart = async function (aRequestNumb, aIsPay) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aRequestNumb: ${aRequestNumb}, aIsPay: ${aIsPay}}`);
    return (await client.IsPayShoppingCartAsync({
        aRequestNumb: aRequestNumb,
        aIsPay: aIsPay
    }))[0];
};

/**
 * Получить данные корзины клиента
 * @param aClID - id пользователя
 * @param aIsPay - признак оплаты заказа
 * @param aDPay - дата оплаты заказа
 * @param aIsClose - признак закрыто
 * @param aDClose - дата закрытия заказа
 * @returns {array}
 */
exports.getProdClientsDataList = async function (aClID, aIsPay, aIsClose, aDPay = '1900-01-01', aDClose = '1900-01-01') {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aClID: ${aClID}, aIsPay: ${aIsPay}, aIsClose: ${aIsClose}, aDPay: ${aDPay}, aDClose: ${aDClose}}`);
    return (await client.GetProdClientsDataListAsync({
        aClID: aClID,
        aIsPay: aIsPay,
        aDPay: aDPay,
        aIsClose: aIsClose,
        aDClose: aDClose
    }))[0].GetProdClientsDataListResult;
};

/**
 * Выдача данных по ID товара
 * @param aID
 * @returns {array}
 */

exports.getProductIDData = async function (aID) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aID: ${aID}}`);
    return (await client.GetProductIDDataAsync({
        aID: aID
    }))[0].GetProductIDDataResult.ProductsData[0];
};

/**
 * Установка способа оплаты заказа клиентом (покупателем)
 * @param aRequestNumb - Номер общего заказа клиента (покупателя).
 * @param aRequestPay - Способ оплаты заказа. 0 - не определено (если надо сбросить способ оплаты), 1 - оплата сейчас, 2 - наличными в магазине в магазине.
 * @returns {array}
 *
 * SetRequestPayResult - состояние
 */
exports.setRequestPay = async function (aRequestNumb, aRequestPay) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aRequestNumb: ${aRequestNumb}, aRequestPay: ${aRequestPay}}`);
    return (await client.SetRequestPayAsync({
        aRequestNumb: aRequestNumb,
        aRequestPay: aRequestPay
    })[0].SetRequestPayResult);
};

/**
 * Установка признака ЗАБЕРУ В МАГАЗИНЕ / ВЫНЕСТИ К МАШИНЕ
 * @param aRequestNumb - Номер общего заказа клиента (покупателя).
 * @param aCarShop - Признак "Вынести к машине / заберу сам". 0 - не определено (если надо сбросить способ получения заказа), 1 - вынести к машине, 2 - заберу сам.
 * @returns {array}
 *
 * SetCarShopResult - True если функция выполнена успешно, false - если нет.
 * aAuto - Марка автомобиля клиента.
 * aColorAuto - Цвет автомобиля клиента.
 * aNumbAuto - Номер автомобиля клиента.
 */
exports.setCarShop = async function (aRequestNumb, aCarShop) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aRequestNumb: ${aRequestNumb}, aCarShop: ${aCarShop}}`);
    return (await client.SetCarShopAsync({aRequestNumb: aRequestNumb, aCarShop: aCarShop})[0].SetCarShopResult);
};