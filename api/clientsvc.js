const soap = require('soap');
const logger = require('../winston').logger;

let client;

exports.init = async function () {
    const wdsl = 'https://beer.graysonhunt.ru/ClientsSvc.asmx?wsdl';
    client = await soap.createClientAsync(wdsl);
    logger.debug('Active: ' + wdsl);
};

const path = require('path');
const scriptName = path.basename(__filename);

/**
 *
 * @param aName - как называть пользователя (обязательное)
 * @param aPhone - телефон пользователя (обязательное)
 * @param aBirthday - дата рождения (по-умолчанию 1900-01-01 будет равно нулю)
 * @param aAuto - Автомобиль
 * @param aColorAuto - Цвет автомобиля
 * @param aNumbAuto - Номер автомобиля
 * @param aIs18Year - Есть ли 18 лет (по-умолчанию false)
 * @param aPass - пароль, длинной  от 5 до 15 символов
 * @returns {array}
 *
 * AddClientResult - состояние
 * aID - уникальный идентификатор клиента
 */
exports.addClient = async function (aName = '', aPhone = '', aPass = '', aBirthday = '1900-01-01T00:00:00.000Z', aAuto = '', aColorAuto = '', aNumbAuto = '', aIs18Year = true) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aName: '${aName}', aPhone: '${aPhone}', aBirthday: '${aBirthday}', aAuto: ${aAuto}, aColorAuto: ${aColorAuto},
    aNumbAuto: ${aNumbAuto}, als18Year: ${aIs18Year}, aPass: '${aPass}'}`);
    return (
        await client.AddClientAsync({
            aName: aName,
            aPhone: aPhone,
            aBirthday: aBirthday,
            aAuto: aAuto,
            aColorAuto: aColorAuto,
            aNumbAuto: aNumbAuto,
            aIs18Year: aIs18Year,
            aPass: aPass,
        }))[0];
};


/**
 * @param aID - уникальный идентификатор пользователя
 * @param aName - как называть пользователя (обязательное)
 * @param aPhone - телефон пользователя (обязательное)
 * @param aBirthday - дата рождения
 * @param aAuto - Автомобиль
 * @param aColorAuto - Цвет автомобиля
 * @param aNumbAuto - Номер автомобиля
 * @param aIs18Year - Есть ли 18 лет (по-умолчанию false)
 * @returns {array}
 *
 * EdtClientResult - состояние
 */
exports.edtClient = async function (aID, aName, aPhone, aBirthday, aAuto, aColorAuto, aNumbAuto, aIs18Year) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aID: ${aID}, aName: ${aName}, aPhone: ${aPhone}, aBirthday: ${aBirthday}, aAuto: ${aAuto}, 
    aColorAuto: ${aColorAuto}, aNumbAuto: ${aNumbAuto}, aIs18Year: ${aIs18Year}}`);

    return (await client.EdtClientAsync({
        aID: aID,
        aName: aName,
        aPhone: aPhone,
        aBirthday: aBirthday,
        aAuto: aAuto,
        aColorAuto: aColorAuto,
        aNumbAuto: aNumbAuto,
        aIs18Year: aIs18Year
    }))[0];
};

/**
 * @param aID - уникальный идентификатор пользователя
 * @param aIsBlock - состояние клиент (блок, анблок)
 * @returns {array}
 *
 * SetClientIsBlockResult - состояние
 */
exports.setClientIsBlock = async function (aID, aIsBlock) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aID: ${aID}, aIsBlock: ${aIsBlock}}`);
    return (await client.SetClientIsBlockAsync({aID: aID, aIsBlock: aIsBlock}))[0];
};


/**
 * @param aPhone - уникальный идентификатор пользователя
 * @param aPass - пароль пользователя
 * @returns {array}
 *
 * ClientConnectResult - состояние
 * aID - ID пользователя
 * aBlock - Заблокирован ли пользователь
 * aName - Имя пользователя
 * aNotPass - Правильно ли введен пароль
 * a18Year - Есть ли 18 лет?
 */
exports.clientConnect = async function (aPhone, aPass) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aPhone: ${aPhone}, aPass: ${aPass}}`);
    return (await client.ClientConnectAsync({aPhone: aPhone, aPass: aPass}))[0];
};

/**
 * @param aClientID - id клиента
 * @param aPassword - новый пароль пользователя
 * @returns {array}
 *
 * ClientEdtPasswordResult - состояние
 */
exports.clientEditPassword = async function (aClientID, aPassword) {
    logger.info(`${Object.keys(this)[1]}`);
    logger.debug(`{aClientID: ${aClientID}, aPassword: ${aPassword}}`);
    return (await client.ClientEdtPasswordAsync({aClientID: aClientID, aPassword: aPassword}))[0];
};