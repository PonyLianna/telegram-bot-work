const Sequelize = require('sequelize');
const sequelize = new Sequelize('telegram', 'root', '', {
    dialect: 'mysql',
    host: '127.0.0.1',
    port: '3306',
});

const User = sequelize.define('user', {
    id: {type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true},
    fio: Sequelize.STRING,
    phoneNum: Sequelize.INTEGER,
    cardNum: Sequelize.STRING
});

const Product = sequelize.define('product', {
    id: {type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true},
    name: Sequelize.STRING,
    picture: Sequelize.STRING,
    price: Sequelize.INTEGER
});

const Cart = sequelize.define('cart', {
    id: {type: Sequelize.INTEGER, autoIncrement: true, primaryKey: true},
    name: {type: Sequelize.STRING},
    // picture: Sequelize.STRING,
    productId: Sequelize.INTEGER
});

const Cheque = sequelize.define('cheque', {
    // id: {type: Sequelize.INTEGER, autoIncrement: true},
    name: Sequelize.STRING,
    // picture: Sequelize.STRING,
    productId: Sequelize.INTEGER,
    cost: Sequelize.INTEGER
});

sequelize.sync({force: true});
// sequelize.sync();

module.exports = {
    User, Product, Cart, Cheque
};