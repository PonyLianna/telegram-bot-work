const Telegraf = require('telegraf');
const Extra = require('telegraf/extra');
const Markup = require('telegraf/markup');


const fs = require('fs');

// Конфигурация
const config = require('./config/config.json');

const SocksAgent = require('socks5-https-client/lib/Agent');
const socksAgent = new SocksAgent({
    socksHost: config.socks.host,
    socksPort: config.socks.port,
    socksUsername: config.socks.username,
    socksPassword: config.socks.password,
});


global['modules'] = [];
global['ids'] = {};
global['logger'] = require('./winston').logger;

// Инициализация главных частей модулей init.js
fs.readdir('modules', (err, modules) => {
    modules.forEach(async (name) => {
        logger.debug(`./modules/${name}/init.js подключен`);
        global['modules'].push(require(`./modules/${name}/init.js`));
        console.log(modules);
    });
});

// Инициализируем подключение по SOAP
fs.readdir('api', (err, files) => {
    files.forEach(async (file) => {
        logger.debug(`${file} подключен`);
        await require(`./api/${file}`).init();
        global[file.replace('.js', '')] = require(`./api/${file}`)
    })
});

// Конфигурация telegram
const telegram_configuration = {polling: true};
if (config.socks.host) telegram_configuration.telegram = {agent: socksAgent};

// Инициализация подключения к telegram
const bot = new Telegraf(config.api_key, telegram_configuration);

// Поприветствуем на /start
bot.start((ctx) => {
    // console.log(ctx.update.message);//
    logger.debug(ctx.update.message);
    ctx.reply(`Приветствуем Вас, в нашем чудесном заведении!\nМы предлагаем Вам возможность ознакомиться ` +
    `с нашим *меню*, сделать предварительный *заказ*, _чтобы не терять времени на очередь_, а также _получить скидку_. \n` +
    `Для Вашего удобства, Вы можете сразу заполнить необходимые данные, чтобы мы Вас запомнили и процесс заказа стал `+
    `еще _быстрее_!`, {
        parse_mode: "Markdown", reply_markup: Markup
            .keyboard(['🛒 Меню', '📝 Запомнить меня'], {columns: 2})
            .resize()
    });
});

// На сообщение или запрос
bot.on(['message', 'callback_query'], (ctx) => {
    for (const module of global['modules']) {
        module(bot, ctx)
    }
});

bot.startPolling();

module.exports = {
    Extra, Markup, Telegraf
};